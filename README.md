# hoppr-demo

In this demo we'll progress from sbom creation to running hoppr to collect and package our tools and resources.

# System requirements

- python 3.10 or greater

Depending on which sub demo:

- helm
- git
- docker
- maven
- pypi
- yum