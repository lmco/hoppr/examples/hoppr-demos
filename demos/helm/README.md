# Helm Demo

This demo will walk you through how to collect helm charts and bundle them up for transfering.

## Requirements

Ensure that the following are installed for use:

- Python 3.10 or greater
- Helm v3

## Components of Demo

__Transfers.yml__: A declaritive file that instructs hoppr how to process plugins and in which order.  For this demo you'll note that there are two plugins, one for helm in the collection phase and one for bundling the collection into a tar.

__Manifest.yml__: A delaritive file that defines which repositories hoppr should pull resources from, as well as the location of the CycloneDX formatted sbom.

## Running hoppr

Running hoppr is as simple as first installing and then running the collection.

```bash

pip install hoppr

hopctl bundle --transfer transfer.yml manifest.yml

```

Your bundle will be stored at `$PWD/bundle.tar.gz`